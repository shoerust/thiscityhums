from django.contrib import admin
from index.models import CodePost, WritePost, MusicPost, MiscPost, Comment


class CommonMedia:
    js = (
        'https://ajax.googleapis.com/ajax/libs/dojo/1.6.0/dojo/dojo.xd.js',
        'http://static.southers.id.au/static/thiscityhums/js/editor.js',
    )
    css = {
        'all': ('http://static.southers.id.au/static/thiscityhums/css/editor.css',)
    }


class CodePostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'abstract', 'content']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
        ]
    list_display = ('title', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['title', 'abstract', 'content']
    date_hierarchy = 'pub_date'

admin.site.register(CodePost,
                    CodePostAdmin,
                    Media=CommonMedia)


class WritePostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'abstract', 'content']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
        ]
    list_display = ('title', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['title', 'abstract', 'content']
    date_hierarchy = 'pub_date'

admin.site.register(WritePost,
                    WritePostAdmin,
                    Media=CommonMedia)


class MusicPostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'abstract', 'content']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
        ]
    list_display = ('title', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['title', 'abstract', 'content']
    date_hierarchy = 'pub_date'

admin.site.register(MusicPost,
                    MusicPostAdmin,
                    Media=CommonMedia)


class MiscPostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'abstract', 'content']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
        ]
    list_display = ('title', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['title', 'abstract', 'content']
    date_hierarchy = 'pub_date'

admin.site.register(MiscPost,
                    MusicPostAdmin,
                    Media=CommonMedia)

class CommentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['post_type_name', 'post_id', 'username', 'website', 'content']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
        ]
    list_display = ('post_type_name', 'post_id', 'username', 'website', 'content', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    date_hierarchy = 'pub_date'

admin.site.register(Comment,
                    CommentAdmin,
                    Media=CommonMedia)
