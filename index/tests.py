import datetime
from django.test import TestCase
from django.utils import timezone

from index.models import CodePost, WritePost, MusicPost

class CodePostMethodTests(TestCase):
    def test_was_published_recently_with_future_poll(self):
        """
        was_published_recently() should return false for polls whose
        pub_date is in the future
        """
        future_post = CodePost(pub_date=timezone.now() + datetime.timedelta(days=30))
        self.assertEqual(future_post.was_published_recently(), False)

    def test_was_published_recently_with_old_poll(self):
        """
        was_published_recently() should return False for polls whose pub_date
        is older than 1 day
        """
        old_post = CodePost(pub_date=timezone.now() - datetime.timedelta(days=30))
        self.assertEqual(old_post.was_published_recently(), False)

    def test_was_published_recently_with_recent_poll(self):
        """
        was_published_recently() should return True for polls whose pub_date
        is within the last day
        """
        recent_post = CodePost(pub_date=timezone.now() - datetime.timedelta(hours=1))
        self.assertEqual(recent_post.was_published_recently(), True)

class WritePostMethodTests(TestCase):
    def test_was_published_recently_with_future_poll(self):
        """
        was_published_recently() should return false for polls whose
        pub_date is in the future
        """
        future_post = WritePost(pub_date=timezone.now() + datetime.timedelta(days=30))
        self.assertEqual(future_post.was_published_recently(), False)

    def test_was_published_recently_with_old_poll(self):
        """
        was_published_recently() should return False for polls whose pub_date
        is older than 1 day
        """
        old_post = WritePost(pub_date=timezone.now() - datetime.timedelta(days=30))
        self.assertEqual(old_post.was_published_recently(), False)

    def test_was_published_recently_with_recent_poll(self):
        """
        was_published_recently() should return True for polls whose pub_date
        is within the last day
        """
        recent_post = WritePost(pub_date=timezone.now() - datetime.timedelta(hours=1))
        self.assertEqual(recent_post.was_published_recently(), True)

class MusicPostMethodTests(TestCase):
    def test_was_published_recently_with_future_poll(self):
        """
        was_published_recently() should return false for polls whose
        pub_date is in the future
        """
        future_post = MusicPost(pub_date=timezone.now() + datetime.timedelta(days=30))
        self.assertEqual(future_post.was_published_recently(), False)

    def test_was_published_recently_with_old_poll(self):
        """
        was_published_recently() should return False for polls whose pub_date
        is older than 1 day
        """
        old_post = MusicPost(pub_date=timezone.now() - datetime.timedelta(days=30))
        self.assertEqual(old_post.was_published_recently(), False)

    def test_was_published_recently_with_recent_poll(self):
        """
        was_published_recently() should return True for polls whose pub_date
        is within the last day
        """
        recent_post = MusicPost(pub_date=timezone.now() - datetime.timedelta(hours=1))
        self.assertEqual(recent_post.was_published_recently(), True)

class MiscPostMethodTests(TestCase):
    def test_was_published_recently_with_future_poll(self):
        """
        was_published_recently() should return false for polls whose
        pub_date is in the future
        """
        future_post = MusicPost(pub_date=timezone.now() + datetime.timedelta(days=30))
        self.assertEqual(future_post.was_published_recently(), False)

    def test_was_published_recently_with_old_poll(self):
        """
        was_published_recently() should return False for polls whose pub_date
        is older than 1 day
        """
        old_post = MusicPost(pub_date=timezone.now() - datetime.timedelta(days=30))
        self.assertEqual(old_post.was_published_recently(), False)

    def test_was_published_recently_with_recent_poll(self):
        """
        was_published_recently() should return True for polls whose pub_date
        is within the last day
        """
        recent_post = MusicPost(pub_date=timezone.now() - datetime.timedelta(hours=1))
        self.assertEqual(recent_post.was_published_recently(), True)