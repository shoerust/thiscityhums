
from django.db import models
from django import forms
import datetime
from django.utils import timezone

class CodePost(models.Model):
    title = models.CharField(max_length=200)
    abstract = models.TextField()
    content = models.TextField()
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.title

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class WritePost(models.Model):
    title = models.CharField(max_length=200)
    abstract = models.TextField()
    content = models.TextField()
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.title

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class MusicPost(models.Model):
    title = models.CharField(max_length=200)
    abstract = models.TextField()
    content = models.TextField()
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.title

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class MiscPost(models.Model):
    title = models.CharField(max_length=200)
    abstract = models.TextField()
    content = models.TextField()
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.title

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class Comment(models.Model):
    post_type_name = models.CharField(max_length=20)
    post_id = models.IntegerField()
    username = models.CharField(max_length=50)
    website = models.CharField(max_length=200)
    content = models.TextField()
    pub_date = models.DateTimeField('date_published')
    def __unicode__(self):
        return self.username

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date

class CommentForm(forms.Form):
    username = forms.CharField(required=True, max_length=50)
    website = forms.URLField(required=False, max_length=200)
    content = forms.CharField(widget=forms.Textarea, required=True)

