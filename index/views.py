from django.shortcuts import render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from urlparse import urlparse
import urllib, hashlib
import datetime
from index.models import CodePost, WritePost, MusicPost, MiscPost, Comment, CommentForm

def index(request):
    music_post = MusicPost.objects.all().order_by('-pub_date')[:1]
    write_post = WritePost.objects.all().order_by('-pub_date')[:1]
    code_post = CodePost.objects.all().order_by('-pub_date')[:1]
    misc_post = MiscPost.objects.all().order_by('-pub_date')[:1]
    return render_to_response('index/index.html', {"music_post": music_post,
                                                   "write_post": write_post,
                                                   "code_post": code_post,
                                                   "misc_post": misc_post},
        context_instance=RequestContext(request))
def about(request):
    email = "william@southers.id.au"
    default = "http://www.example.com/default.jpg"
    size = 120
    gravatar_url = "http://www.gravatar.com/avatar/" + hashlib.md5(email.lower()).hexdigest() + "?"
    gravatar_url += urllib.urlencode({'d':default, 's':str(size)})
    return render_to_response('about/index.html', {"gravatar_url": gravatar_url},
        context_instance=RequestContext(request))

def details(request, post_type_name, post_id):
    form = CommentForm
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            url = urlparse(form.cleaned_data['website'])
            new_comment = Comment()
            new_comment.post_id = post_id
            new_comment.post_type_name = post_type_name
            new_comment.pub_date = datetime.datetime.today()
            new_comment.username = form.cleaned_data['username']
            if url.scheme:
                new_comment.website = url.geturl()
            else:
                if url.geturl():
                    new_comment.website = 'http://' + url.geturl()
            new_comment.content = form.cleaned_data['content']
            new_comment.save()

    if post_type_name == "code":
        post_type = code_specific
    if post_type_name == "music":
        post_type = music_specific
    if post_type_name == "writing":
        post_type = writing_specific
    if post_type_name == "misc":
        post_type = misc_specific
    post = post_specific(post_type, post_id)
    comment_list = comments(post_type_name, post_id)
    paginator = Paginator(comment_list, 5)

    page = request.GET.get('page')
    try:
        viewable_comments = paginator.page(page)
    except PageNotAnInteger:
        viewable_comments = paginator.page(1)
    except EmptyPage:
        viewable_comments = paginator.page(paginator.num_pages)

    return render_to_response(post_type_name + '/details.html', {"post": post,
                                                    "viewable_comments": viewable_comments,
                                                    "form": form},
                                context_instance=RequestContext(request))
def history(request, post_type):
    all_posts = posts(post_type)
    paginator = Paginator(all_posts, 5)

    page = request.GET.get('page')
    try:
        viewable_posts = paginator.page(page)
    except PageNotAnInteger:
        viewable_posts = paginator.page(1)
    except EmptyPage:
        viewable_posts = paginator.page(paginator.num_pages)

    return render_to_response('common/history.html', {"viewable_posts": viewable_posts, "post_type": post_type.__name__ },
        context_instance=RequestContext(request))

def posts(data_function):
    return data_function()

def post_specific(data_function, post_id):
    return data_function(post_id)

def writing():
    return WritePost.objects.all()

def writing_specific(post_id):
    return WritePost.objects.get(pk=post_id)

def code():
    return CodePost.objects.all()

def code_specific(post_id):
    return CodePost.objects.get(pk=post_id)

def music():
    return MusicPost.objects.all()

def music_specific(post_id):
    return MusicPost.objects.get(pk=post_id)

def misc():
    return MiscPost.objects.all()

def misc_specific(post_id):
    return MiscPost.objects.get(pk=post_id)

def comments(post_type_name, post_id):
    return Comment.objects.filter(post_type_name=post_type_name, post_id=post_id).order_by('-pub_date')


