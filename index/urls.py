from django.conf.urls import patterns, url
from django.conf import settings
from django.views.generic import ListView
from django.utils import timezone
from index import views
from index.models import WritePost, CodePost, MusicPost, MiscPost
from index.views import code, writing, music, misc

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.about, name='about_index'),
    url(r'^(?P<post_type_name>[a-z]{0,10})/(?P<post_id>\d+)/details/$', views.details,
        name='details'),
    # Code pages *******************************************************
    url(r'^code/$',
       ListView.as_view(
           queryset=CodePost.objects.filter(pub_date__lte=timezone.now).order_by('-pub_date')[:5],
           context_object_name='latest_code_post_list',
           template_name='code/index.html'),
       name='code_index'),
    url(r'^code/history/$', views.history, {'post_type': code}, name='code_history'),
    # Writing pages ***************************************************
    url(r'^writing/$',
       ListView.as_view(
           queryset=WritePost.objects.filter(pub_date__lte=timezone.now).order_by('-pub_date')[:5],
           context_object_name='latest_write_post_list',
           template_name='writing/index.html'),
       name='write_index'),
    url(r'^writing/history/$', views.history, {'post_type': writing}, name='write_history'),
    # Music pages ******************************************************
    url(r'^music/$',
       ListView.as_view(
           queryset=MusicPost.objects.filter(pub_date__lte=timezone.now).order_by('-pub_date')[:5],
           context_object_name='latest_music_post_list',
           template_name='music/index.html'),
       name='music_index'),
    url(r'^music/history/$', views.history, {'post_type': music}, name='music_history'),
    # Misc pages ******************************************************
    url(r'^misc/$',
        ListView.as_view(
            queryset=MiscPost.objects.filter(pub_date__lte=timezone.now).order_by('-pub_date')[:5],
            context_object_name='latest_misc_post_list',
            template_name='misc/index.html'),
        name='misc_index'),
    url(r'^misc/history/$', views.history, {'post_type': misc}, name='misc_history'),
   )

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
